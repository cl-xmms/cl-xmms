;;; CL-XMMS --- Common Lisp bindings for libxmms
;;; version 0.1
;;;
;;; (C) 2005 Vikas Gorur P <vikasgp@gmail.com>
;;;
;;; You are granted the rights to distribute and use this software
;;; as governed by the terms of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html), also known as the LLGPL.

;;; TODO: The API can be made much nicer, e.g., volume,
;;;       equalizer, etc., can be made generalized variables.

;;; Setup

(defpackage :xmms
  (:use :common-lisp)
  (:export :get-version
;	   :playlist
;	   :playlist-add
	   :playlist-delete
	   :play
	   :pause
	   :stop
	   :playing?
	   :paused?
	   :get-playlist-position
	   :set-playlist-position
	   :get-playlist-length
	   :playlist-clear
	   :get-output-time
	   :jump-to-time
	   :get-volume
	   :get-main-volume
	   :get-balance
	   :set-volume
	   :set-main-volume
	   :set-balance
	   :get-skin
	   :set-skin
	   :get-playlist-file
	   :get-playlist-title
	   :get-playlist-time
	   :get-info
	   :main-window-toggle
	   :equalizer-window-toggle
	   :playlist-window-toggle
	   :is-main-window?
	   :is-equalizer-window?
	   :is-playlist-window?
	   :show-preferences-box
	   :toggle-always-on-top
	   :eject
	   :playlist-previous
	   :playlist-next
	   :playlist-add-url-string
	   :running?
	   :toggle-repeat
	   :toggle-shuffle
	   :repeat?
	   :shuffle?
;	   :get-equalizer
	   :get-equalizer-preamp
	   :get-equalizer-band
;	   :set-equalizer
	   :set-equalizer-preamp
	   :set-equalizer-band
	   :quit
	   :play-pause
	   :playlist-insert-url-string
	   )
  (:documentation "CL bindings for libxmms"))

(in-package :xmms)

(defparameter *xmms-library-path* #p"/usr/lib/libxmms.so.1")
(require :uffi)
(uffi:load-foreign-library *xmms-library-path*)

;;; Truth conversion utilities

(defun lisp-truth (truth-value)
  (if (= truth-value 0)
      nil
      t))

(defun c-truth (truth-value)
  (if truth-value
      1
      0))

;;; Bindings

;--------------------
(uffi:def-function "xmms_remote_playlist"
    ((session :int)
     (plist (* :cstring))
     (num :int)
     (enqueue :int))
  :returning :void)

;; (defun playlist (plist &optional (enqueue nil) (session 0))
;;   (let* ((n (length plist))
;; 	(cplist (uffi:allocate-foreign-object :cstring n)))
;;     (mapcar #'(lambda (item i)
;; 		(setf (uffi:deref-array cplist :cstring i)
;; 		      (uffi:convert-to-cstring item)))
;; 	    plist
;; 	    (loop for i from 0 to (1- n) collect i))
;;     (xmms-remote-playlist session cplist n (c-truth enqueue))
;;     (uffi:free-foreign-object cplist)
;;     (values)))
    
;--------------------
(uffi:def-function "xmms_remote_get_version"
    ((session :int))
  :returning :int)

(defun get-version (&optional (session 0))
  (xmms-remote-get-version session))

;--------------------
;(uffi:def-function "xmms_remote_playlist_add")

;--------------------
(uffi:def-function "xmms_remote_playlist_delete"
    ((session :int)
     (pos :int))
  :returning :void)

(defun playlist-delete (pos &optional (session 0))
  (xmms-remote-playlist-delete session pos)
  (values))

;--------------------
(uffi:def-function "xmms_remote_play"
    ((session :int))
  :returning :void)

(defun play (&optional (session 0))
  (xmms-remote-play session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_pause"
    ((session :int))
  :returning :void)

(defun pause (&optional (session 0))
  (xmms-remote-pause session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_stop"
    ((session :int))
  :returning :void)

(defun stop (&optional (session 0))
  (xmms-remote-stop session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_is_playing"
    ((session :int))
  :returning :int)

(defun playing? (&optional (session 0))
  (lisp-truth (xmms-remote-is-playing session)))

;--------------------
(uffi:def-function "xmms_remote_is_paused"
    ((session :int))
  :returning :int)

(defun paused? (&optional (session 0))
  (lisp-truth (xmms-remote-is-paused session)))

;--------------------
(uffi:def-function "xmms_remote_get_playlist_pos"
    ((session :int))
  :returning :int)

(defun get-playlist-position (&optional (session 0))
  (xmms-remote-get-playlist-pos session))

;--------------------
(uffi:def-function "xmms_remote_set_playlist_pos"
    ((session :int)
     (pos :int))
  :returning :void)

(defun set-playlist-position (pos &optional (session 0))
  (xmms-remote-set-playlist-pos session pos)
  (values))

;--------------------
(uffi:def-function "xmms_remote_get_playlist_length"
    ((session :int))
  :returning :int)

(defun get-playlist-length (&optional (session 0))
  (xmms-remote-get-playlist-length session))

;--------------------
(uffi:def-function "xmms_remote_playlist_clear"
    ((session :int))
  :returning :void)

(defun playlist-clear (&optional (session 0))
  (xmms-remote-playlist-clear session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_get_output_time"
    ((session :int))
  :returning :int)

(defun get-output-time (&optional (session 0))
  (xmms-remote-get-output-time session))

;--------------------
(uffi:def-function "xmms_remote_jump_to_time"
    ((session :int)
     (ms :int))
  :returning :int)

(defun jump-to-time (ms &optional (session 0))
  (xmms-remote-jump-to-time session ms))

;--------------------
(uffi:def-function "xmms_remote_get_volume"
    ((session :int)
     (vl (* :int))
     (vr (* :int)))
  :returning :void)

(defun get-volume (&optional (session 0))
  (let ((pvl (uffi:allocate-foreign-object :int))
	(pvr (uffi:allocate-foreign-object :int)))
    (xmms-remote-get-volume session pvl pvr)
    (let ((vl (uffi:deref-pointer pvl :int))
	  (vr (uffi:deref-pointer pvr :int)))
      (uffi:free-foreign-object pvl)
      (uffi:free-foreign-object pvr)
      (values vl vr))))

;--------------------
(uffi:def-function "xmms_remote_get_main_volume"
    ((session :int))
  :returning :int)

(defun get-main-volume (&optional (session 0))
  (xmms-remote-get-main-volume session))

;--------------------
(uffi:def-function "xmms_remote_get_balance"
    ((session :int))
  :returning :int)

(defun get-balance (&optional (session 0))
  (xmms-remote-get-balance session))

;--------------------
(uffi:def-function "xmms_remote_set_volume"
    ((session :int)
     (vl (* :int))
     (vr (* :int)))
  :returning :void)

(defun set-volume (vl vr &optional (session 0))
  (let ((pvl (uffi:allocate-foreign-object :int))
	(pvr (uffi:allocate-foreign-object :int)))
    (setf (uffi:deref-pointer pvl :int) vl)
    (setf (uffi:deref-pointer pvr :int) vr)
    (xmms-remote-set-volume session pvl pvr)
    (uffi:free-foreign-object pvl)
    (uffi:free-foreign-object pvr)
    (values)))

;--------------------
(uffi:def-function "xmms_remote_set_main_volume"
    ((session :int)
     (v :int))
  :returning :void)

(defun set-main-volume (v &optional (session 0))
  (xmms-remote-set-main-volume session v)
  (values))

;--------------------
(uffi:def-function "xmms_remote_set_balance"
    ((session :int)
     (b :int))
  :returning :void)

(defun set-balance (b &optional (session 0))
  (xmms-remote-set-balance session b)
  (values))

;--------------------
(uffi:def-function "xmms_remote_get_skin"
    ((session :int))
  :returning :cstring)

(defun get-skin (&optional (session 0))
    (uffi:convert-from-cstring (xmms-remote-get-skin session)))

;--------------------
(uffi:def-function "xmms_remote_set_skin"
    ((session :int)
     (skinfile :cstring))
  :returning :void)

(defun set-skin (skinfile &optional (session 0))
  (uffi:with-cstring (cskinfile skinfile)
    (xmms-remote-set-skin session cskinfile)
    (values)))

;--------------------
(uffi:def-function "xmms_remote_get_playlist_file"
    ((session :int)
     (pos :int))
  :returning :cstring)

(defun get-playlist-file (pos &optional (session 0))
  (uffi:convert-from-cstring (xmms-remote-get-playlist-file session pos)))

;--------------------
(uffi:def-function "xmms_remote_get_playlist_title"
    ((session :int)
     (pos :int))
  :returning :cstring)

(defun get-playlist-title (pos &optional (session 0))
  (uffi:convert-from-cstring (xmms-remote-get-playlist-title pos session)))

;--------------------
(uffi:def-function "xmms_remote_get_playlist_time"
    ((session :int)
     (pos :int))
  :returning :int)

(defun get-playlist-time (pos &optional (session 0))
  (xmms-remote-get-playlist-time session pos))

;--------------------
(uffi:def-function "xmms_remote_get_info"
    ((session :int)
     (rate (* :int))
     (freq (* :int))
     (nch (* :int)))
  :returning :void)

(defun get-info (&optional (session 0))
  (let ((prate (uffi:allocate-foreign-object :int))
	(pfreq (uffi:allocate-foreign-object :int))
	(pnch (uffi:allocate-foreign-object :int)))
    (xmms-remote-get-info session prate pfreq pnch)
    (let ((rate (uffi:deref-pointer prate :int))
	  (freq (uffi:deref-pointer pfreq :int))
	  (nch (uffi:deref-pointer pnch :int)))
      (uffi:free-foreign-object prate)
      (uffi:free-foreign-object pfreq)
      (uffi:free-foreign-object pnch)
      (values rate freq nch))))

;--------------------
(uffi:def-function "xmms_remote_main_win_toggle"
    ((session :int)
     (show :int))
  :returning :void)

(defun main-window-toggle (show &optional (session 0))
  (xmms-remote-main-win-toggle session (c-truth show))
  (values))

;--------------------
(uffi:def-function "xmms_remote_eq_win_toggle"
    ((session :int)
     (show :int))
  :returning :void)

(defun equalizer-window-toggle (show &optional (session 0))
  (xmms-remote-eq-win-toggle session (c-truth show))
  (values))

;--------------------
(uffi:def-function "xmms_remote_pl_win_toggle"
    ((session :int)
     (show :int))
  :returning :void)

(defun playlist-window-toggle (show &optional (session 0))
  (xmms-remote-pl-win-toggle session (c-truth show))
  (values))

;--------------------
(uffi:def-function "xmms_remote_is_main_win"
    ((session :int))
  :returning :int)

(defun is-main-window? (&optional (session 0))
  (lisp-truth (xmms-remote-is-main-win session)))

;--------------------
(uffi:def-function "xmms_remote_is_eq_win"
    ((session :int))
  :returning :int)

(defun is-equalizer-window? (&optional (session 0))
  (lisp-truth (xmms-remote-is-eq-win session)))

;--------------------
(uffi:def-function "xmms_remote_is_pl_win"
    ((session :int))
  :returning :int)

(defun is-playlist-window? (&optional (session 0))
  (lisp-truth (xmms-remote-is-pl-win session)))

;--------------------
(uffi:def-function "xmms_remote_show_prefs_box"
    ((session :int))
  :returning :void)

(defun show-preferences-box (&optional (session 0))
  (xmms-remote-show-prefs-box session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_toggle_aot"
    ((session :int)
     (ontop :int))
  :returning :void)

(defun toggle-always-on-top (on-top &optional (session 0))
  (xmms-remote-toggle-aot session (c-truth on-top)))

;--------------------
(uffi:def-function "xmms_remote_eject"
    ((session :int))
  :returning :void)

(defun eject (&optional (session 0))
  (xmms-remote-eject session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_playlist_prev"
    ((session :int))
  :returning :void)

(defun playlist-previous (&optional (session 0))
  (xmms-remote-playlist-prev session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_playlist_next"
    ((session :int))
  :returning :void)

(defun playlist-next (&optional (session 0))
  (xmms-remote-playlist-next session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_playlist_add_url_string"
    ((session :int)
     (string :cstring))
  :returning :void)

(defun playlist-add-url-string (string &optional (session 0))
  (uffi:with-cstring (cstring string)
    (xmms-remote-playlist-add-url-string session cstring)
    (values)))

;--------------------
(uffi:def-function "xmms_remote_is_running"
    ((session :int))
  :returning :int)

(defun running? (&optional (session 0))
  (lisp-truth (xmms-remote-is-running session)))

;--------------------
(uffi:def-function "xmms_remote_toggle_repeat"
    ((session :int))
  :returning :void)

(defun toggle-repeat (&optional (session 0))
  (xmms-remote-toggle-repeat session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_toggle_shuffle"
    ((session :int))
  :returning :void)

(defun toggle-shuffle (&optional (session 0))
  (xmms-remote-toggle-shuffle session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_is_repeat"
    ((session :int))
  :returning :int)

(defun repeat? (&optional (session 0))
  (lisp-truth (xmms-remote-is-repeat session)))

;--------------------
(uffi:def-function "xmms_remote_is_shuffle"
    ((session :int))
  :returning :int)

(defun shuffle? (&optional (session 0))
  (lisp-truth (xmms-remote-is-shuffle session)))

;--------------------
(uffi:def-function "xmms_remote_get_eq"
    ((session :int)
     (preamp (* :float))
     (bands (* (* :float))))
  :returning :void)

;; (defun get-equalizer (&optional (session 0))
;;   (let* ((ppreamp (uffi:allocate-foreign-object :float))
;; 	 (syms (loop for i from 0 to 9
;; 		     collect (gensym)))
;; 	 (cbands (mapcar #'(lambda (sym)
;; 			     (set sym (uffi:allocate-foreign-object :float))
;; 			     (symbol-value sym))
;; 			 syms))
;; 	 (pbands (uffi:allocate-foreign-object '(* :float) 10)))

;;     (mapcar #'(lambda (i ptr)
;; 		(setf (uffi:deref-array pbands '(* :float) i)
;; 		      ptr))
;; 		(loop for i from 0 to 9)
;; 		cbands)

;;     (xmms-remote-get-eq session ppreamp pbands)
;;     (let ((preamp (uffi:deref-pointer ppreamp :float))
;; 	  (bands (mapcar #'(lambda (i)
;; 			     (uffi:deref-array cbands :float i))
;; 			 (loop for i from 0 to 9 collect i))))
;;       (values preamp bands))))
	
;--------------------
(uffi:def-function "xmms_remote_get_eq_preamp"
    ((session :int))
  :returning :float)

(defun get-equalizer-preamp (&optional (session 0))
  (xmms-remote-get-eq-preamp session))

;--------------------
(uffi:def-function "xmms_remote_get_eq_band"
    ((session :int)
     (band :int))
  :returning :float)

(defun get-equalizer-band (band &optional (session 0))
  (xmms-remote-get-eq-band session band))

;--------------------
(uffi:def-function "xmms_remote_set_eq"
    ((session :int)
     (preamp :float)
     (bands (* :float)))
  :returning :void)

;--------------------
(uffi:def-function "xmms_remote_set_eq_preamp"
    ((session :int)
     (preamp :float))
  :returning :void)

(defun set-equalizer-preamp (preamp &optional (session 0))
  (xmms-remote-set-eq-preamp session preamp)
  (values))

;--------------------
(uffi:def-function "xmms_remote_set_eq_band"
    ((session :int)
     (band :float))
  :returning :void)

(defun set-equalizer-band (band &optional (session 0))
  (xmms-remote-set-eq-band session band)
  (values))

;--------------------
(uffi:def-function "xmms_remote_quit"
    ((session :int))
  :returning :void)

(defun quit (&optional (session 0))
  (xmms-remote-quit session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_play_pause"
    ((session :int))
  :returning :void)

(defun play-pause (&optional (session 0))
  (xmms-remote-play-pause session)
  (values))

;--------------------
(uffi:def-function "xmms_remote_playlist_ins_url_string"
    ((session :int)
     (string :cstring)
     (pos :int))
  :returning :void)

(defun playlist-insert-url-string (string pos &optional (session 0))
  (uffi:with-cstring (cstring string)
    (xmms-remote-playlist-ins-url-string session cstring pos)
    (values)))

  
